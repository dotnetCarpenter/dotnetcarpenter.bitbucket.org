/**
 * Pop up Questionnaire
 * @author <jon.ronnenberg@gmail.com>
 * @version 1.0.1
 * NOTE: Does not support IE8 and below
 * Tested in: Chrome 35.0, Firefox 30.0, Internet Explorer 11, Opera 22.0, Safari 7.0.4, Safari iOS 6.1, Android 4.2
 */
(function(win, doc) {

	if( hasRun() )
		return; // survey has already been displayed

	// add flag to removeReadyListeners
	removeReadyListeners.hasRun = false;

	// detect ready state
	if ( doc.readyState === "complete" ) {
		// Handle it asynchronously to allow scripts the opportunity to delay ready
		win.setTimeout( survey, 0 );
	} else {
		// Use the handy event callback
		doc.addEventListener( "DOMContentLoaded", removeReadyListeners, false );
		// A fallback to window.onload, that will always work
		win.addEventListener( "load", removeReadyListeners, false );
	}

	function removeReadyListeners() {
		// remove document ready listeners
		doc.removeEventListener( "DOMContentLoaded", survey, false );
		win.removeEventListener( "load", survey, false );

		if(!removeReadyListeners.hasRun) {
			removeReadyListeners.hasRun = true;
			survey();
		}
	}

	function hasRun() {
		// if true - survey has already been displayed
		return !!~doc.cookie.indexOf("popup_survey_remote=1");		
	}

	function setDefaults(defaults, options) {
		for (var prop in defaults) {
			if(defaults.hasOwnProperty(prop))
				options[prop] = options[prop] || defaults[prop];
		}
		return options;
	}

	function forEach(list, cb) {
		for (var i = list.length - 1; i >= 0; i--) {
			cb(list[i]);
		};
	}

	function testForLink(el) {
		// test if el is a link or if el has a link as a child
		return el instanceof HTMLAnchorElement || el.querySelector("a") !== null;
	}

	function attach(el, fn) {
		el.addEventListener("click", fn);
	}

	function remove(list, fn) {
		forEach(list, function(el) {
			el.removeEventListener("click", fn);
		});
	}

	function save() {
		doc.cookie = "popup_survey_remote=1; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
	}

	function survey() {
		var linkWrappers = doc.querySelectorAll("ul");

		// set default values
		var options = setDefaults({
			surveyLink: "https://da.surveymonkey.com/s/RVPXW35",
			width: 800,
			height: 460
		}, (win['nordeco_survey'] || {}) );

		// add click handler
		forEach(linkWrappers, function(el) {
			attach(el, function clickHandler(e) {
				if( hasRun() )
					return;

				if(testForLink(e.target)) {
					// halt request from link
					e.preventDefault();

					var surveyWindow = win.open(options.surveyLink, "popup_survey_remote", "scrollbars=yes,width=" + options.width + ",height=" + options.height);
					surveyWindow.focus();
					remove(linkWrappers, clickHandler);
					save();
				}
				// presume request from link
				win.setTimeout(function() {
					if(e.target.href)
						location.href = e.target.href;
				}, 10);
			});
		});
	}
}( this, document ));
