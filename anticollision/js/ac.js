var $ = function (selector) { return document.querySelectorAll(selector); }
var coords = map( $("section"), createDummyCoords );
console.dir(coords);

function map(list, fn) {
  return Array.prototype.map.call(list, fn);
}
function createDummyCoords(elem) {
  return new Rectangle(elem);
}
function Rectangle(elem) {
  this.w = elem.offsetWidth;
  this.h = elem.offsetHeight;
}
Rectangle.prototype.diagonal = 70;