/**
 * @version 2003.9.1
 * @author jon.ronnenberg@gmail.com
 */

var hBird = hBird || {};

hBird.create = function create(config) {
  for(var prop in config) {
    if(config.hasOwnProperty(prop))
      this[prop] = config[prop];
  }
};

hBird.load = function(cb, config) {
  d3.json(config.url, function(error, data) {
    if(error)
      return console.warn(error)
    config.data = data;
    cb(config);
  });
};

hBird.addValue = function(modifier) {
  var firstValue = 0;
  if(modifier && !(modifier instanceof Function))
    throw "modifier must be a function";
  return function(item) {
    return modifier ?
            modifier(firstValue += item.value) :
            firstValue += item.value;
  }
};

hBird.invert = function(modifier) {
  return function(d) {
    return d;//modifier.invert(d);
  }
}

hBird.yDownUpScale = d3.scale.linear()
                .domain([0, 100])
                .range([400, 0]);

hBird.yScale = d3.scale.linear()
                .domain([0, 100])
                .range([0, 400]);

hBird.yCoords = function(data) {
  var intermed = [0].concat(
    data.map(
      hBird.addValue(hBird.yScale)
    )
  );
  return hBird.yCoords.coords = intermed.splice(0, intermed.length-1);
};

hBird.sizes = {
  width: 510,
  getWidth: function getWidth(percent) {
    return this.width * (parseFloat(this.getFloat.exec(percent)[0]) / 100);
  },
  getFloat: /\d+\.?\d+/
};

hBird.initialize = function(cb, config) {
  hBird.yCoords(config.data.values);
  var canvas = d3.select(".chart")
                .append("svg"),
      width  = hBird.sizes.getWidth(canvas.style("width"));
               canvas
                // .style("width", widthparseFloat(getFloat.exec(width)[0]) + 10 + "%")
                //.attr("width", width/*parseFloat(getFloat.exec(width)[0]) + 10 + "%"*/)
                .attr("height", 400)                
                .attr("viewBox", "0 0 " + width +" 400");
    var rect = canvas
                .append("g")
                .attr("transform", "translate(50)") // TODO why doesn't this for the underlaying rects?
                .attr("class", "bar")
                .selectAll("rect")
                .data(config.data.values)
                .enter()
                .append("rect")
                .attr("y", function(d, i) {
                  return hBird.yCoords.coords[i];
                })
                .attr("height", function(d, i) {
                  return hBird.yScale(d.value);
                })
                .attr("width", "100%")
                .attr("class", function(d, i) {
                  return "segment" + (i + 1);
                })
                .each(function(d, i) {
                  this._yCoord = hBird.yCoords.coords[i];
                  this._datum = d;
                })
            canvas
                .selectAll("rect")
                .data(config.data.values)
                .insert("text", "rect")
                .text(function(d) {
                  return d.label;
                })
                .attr("fill", "#000")
                .attr("transform", "translate(1,10)")
                .attr("text-anchor", "right");
   var yAxis = d3.svg.axis()
                .scale(hBird.yDownUpScale)
                .tickSize(1)
                .tickPadding(4)
                .innerTickSize(2)
                .orient("left")
                .tickValues([0, 20, 40, 60, 80, 100])
                .tickFormat(function(d) {
                  return d + "%";
                });
  canvas.append("g")
        .attr("class", "y-axis")
        .attr("transform", "translate(30)")
        .call(yAxis);
  var periodText = d3.select(".period")
                    .text(config.data.period);
  var previous = d3.select(".previous")
                  .on("click", hBird.previous(rect));
  var next     = d3.select(".next")
                  .on("click", hBird.next(rect));
  config.canvas = canvas;
  config.rect = rect;
  config.periodText = periodText;
  cb(config);
};

hBird.draw = function draw(cb, config) {
  hBird.yCoords(config.data.values);
  config.rect
        .data(config.data.values)
        .transition()
        .duration(750)
        .attrTween("y", function(d, i) {
          var inter = d3.interpolate(this._yCoord, hBird.yCoords.coords[i]);
          this._yCoord = hBird.yCoords.coords[i];
          return inter;
        })
        .attrTween("height", function(d) {
          var inter = d3.interpolate(hBird.yScale(this._datum.value), hBird.yScale(d.value));
          this._datum = d;
          return inter;
        })
        .each(function(d, i) {
          d3.select(this)
            .attr("height", hBird.yScale(d.value))
            .attr("y", hBird.yCoords.coords[i]);
        });
        config.periodText.text(config.data.period);
  d3.select(".lost span")
    .text(config.data["Lost to follow up"] + "%")
    .transition()
    .delay(100);
  cb(config);
};

hBird.log = function log(cb, data) {
  console.dir(data);
  cb(data);
};

hBird.previous = function previous(drawing) {
  return function() {
    // this.style.cursor = "progress";
    // drawing.transition().duration(0);
    hBird.move(-1);
    // this.style.cursor = "pointer";
  }
};

hBird.next = function next(drawing) {
  return function() {
    // this.style.cursor = "progress";
    // drawing.transition().duration(0);
    hBird.move(1);
    // this.style.cursor = "pointer";
  }
};

hBird.update = function(object, giveprop, takeprop, fn) {
  var argumentsLength = arguments.length;
  return function update(cb, updated) {
    switch (argumentsLength) {
      case 1: object = updated; break;
      case 2: object[giveprop] = updated; break;
      case 3: object[giveprop] = updated[takeprop]; break;
      case 4: object[giveprop] = fn(updated[takeprop]); break;
      default: console.warn("hBird.update called with weird arguments");
    }
    cb(updated);
  }
};

hBird.move = function(direction) {
  var test = hBird.config.counter.c;
  test += direction;
  if(test < 0 || test >= hBird.config.counter.max) {
    //console.warn("we got to the end!")
    return; // we at the end
  }
  !~direction ? hBird.defer(hBird.backwards(hBird.config)) : hBird.defer(hBird.forwards(hBird.config));
};

hBird.pickData = function(direction, counter) {
  return function pickData(cb, config) {
    config.data = config.dataset[[!~direction ? --counter.c : ++counter.c]];
    cb(config);
  }
};

// Needs further thought
// hBird.indentity = function(cb, data) {
//   cb(cb(data));
// };
hBird.wait = function(wait) {
  return function wait(cb, config) {
    setTimeout(function() {
      cb(config);
    }, wait)
  }
};

hBird.defer = function defer() {
  return hBird.wait(0);
}

hBird.noOp = function noOp(cb, config) { cb; };

(function() {
  var counter = { c:-1, max: 0 },//TODO 0 from url .. -1 is a hack because forwardDataPicker otherwise will initialize the 1st dataset and not the 0th
      forwardDataPicker = hBird.pickData(1, counter),
      backwardsDataPicker = hBird.pickData(-1, counter);

  // setup program
  hBird.config = new hBird.create({ periodText: "", url: "data/data7.json", dataset: null, data: null, canvas: null, rect: null, counter:counter });
  hBird.initialRun = hBird.compose(
    hBird.load,
    hBird.update(counter, "max", "data", function(d) {
      return d.length
    }),
    hBird.update(hBird.config, "dataset", "data"),
    forwardDataPicker,
    hBird.initialize
  );
  hBird.forwards = hBird.composeCycle(
    forwardDataPicker,
    hBird.draw,
    hBird.noOp
  );
  hBird.backwards = hBird.composeCycle(
    backwardsDataPicker,
    hBird.draw,
    hBird.noOp
  );
  // start program
  hBird.initialRun(hBird.config);
}());
