var hBird = hBird || {};

hBird.config = function config(config) {
  for(var prop in config) {
    if(config.hasOwnProperty(prop))
      this[prop] = config[prop];
  }
}

hBird.load = function(cb, config) {
  d3.json(config.url, function(error, data) {
    if(error)
      return console.warn(error)
    config.data = data;
    cb(config);
  });
}

hBird.getValue = function(item) {
  return item.value;
}

hBird.initialize = function(cb, config) {
  var canvas = d3.select(document.body)
                .append("svg")
                .attr("width", 100)
                .attr("height", 400),
      color  = d3.rgb(188, 202, 0),
      yScale = d3.scale.linear()
                .domain([0, 100])
                .range([0, 400]),
      yCoords= config.data.values.map(function(d) {
                  return typeof d.value === "number" ?
                            d.value :
                            d3.sum(d.value, hBird.getValue);
                }),
      stack  = d3.layout.stack()
                  .x(function(d) {
                    return 0;
                  })
                  .y(function(d, i) {
                    return yScale(i === 0 ? 0 : yCoords[i - 1]);                    
                  })
                  .values(function(d) {
                    return d.value instanceof Array ? d.value : [d];
                  }),
        rect = canvas.selectAll("rect")
               .data(stack(config.data.values))
               .enter()
               .append("rect")
               .attr("height", function(d, i) {
               //  var value = d.values[i].value;
                 return yScale(typeof d.value === "number" ?
                            d.value :
                            d3.sum(d.value, hBird.getValue));
                 //return yScale(d.values);
                 //return typeof d.value === "number" ? d.value : d3.merge(d.value).map(hBird.getValue);
               })
               .attr("width", 100)
               .attr("fill", function(d, i) {
                  return color.darker(i);
               });
                //.attr("transform", "translate(0, 400)"),

    /*   rect1 = canvas.append("rect")
                .attr("width", 50)
                .attr("height", 100)
                .attr("y", 26)
                .attr("fill","#2d578b"),
        rect2 = canvas.append("rect")
                .attr("width", 50)
                .attr("height", 60)
                .attr("height", 26)
                .attr("fill","#DBEAF9"),
    */
     /* keys   = Object.keys(data[0].values)
                .concat(Object.keys(data[0].values.Diagnosed)),
      */
  cb({
    data: config.data,
    canvas: canvas,
    yScale: yScale,
    rect: rect
  });
}

hBird.pickData = function(start) {
  var currentDataSet;
  return function(cb, config) {
    if(!currentDataSet) // if config doesn't have a data property then data is config
      currentDataSet = config.data;
    config.data = currentDataSet[start];
    cb(config);
  }
}

hBird.draw = function(cb, config) {
  config.canvas.selectAll.call(config.canvas, "rect")
               .data(config.data)
               .enter()
               .append("rect")
               .attr("y", function(d, i) {
                 return d;
               })
               .attr("x", function(d, i) {
                 return 0;
               })
               .attr("height", function(d) {
                 return config.yScale(d.values);
               })
               .attr("width", 100)
               .attr("fill", "#2d578b");
  cb(config);
}

hBird.log = function log(cb, data) {
  console.dir(data);
  cb(data);
}
var dataPicker = hBird.pickData(4);
var testRun = hBird.compose(hBird.load, hBird.initialize, dataPicker, hBird.log);
var firstTry = hBird.compose(hBird.load, dataPicker, hBird.initialize, hBird.log);
firstTry(new hBird.config({ url: "data/data3.json", data: null, rect: null }));
