/**
 * @version 2013.10.1
 * @author jon.ronnenberg@gmail.com
 */

var hBird = hBird || {};

hBird.iconTable = {
  "Undiagnosed": "drop",
  "Diagnosed, not on ART": "pipette",
  "Lost to follow-up": "question",
  "In ART treatment": "pill",
  "Diseased": "cross"
}

hBird.create = function create(config) {
  "use strict";
  for(var prop in config) {
    if(config.hasOwnProperty(prop))
      this[prop] = config[prop];
  }
};

hBird.load = function(cb, config) {
  "use strict";
  var mimeType = /\.(svg|json)/,
      found = mimeType.exec(config.url);
  switch ( ((found && found[1]) || "") ) {
    case "svg":
      d3.xml(config.url, "image/svg+xml", function(error, xml) {
        if(error)
          return console.warn(error)
        config.iconset = xml;
        cb(config);
      });
      break;
    case "json":
      d3.json(config.url, function(error, data) {
        if(error)
          return console.warn(error)
        config.data = data;
        cb(config);
      });
      break;
    default:
      console.warn("hBird.load: mime-type not found - nothing is loaded");
      cb(config);
  }
};

hBird.sizes = {
  width: 762,//d3.select(".chart").node().clientWidth,
  height: 690,//675,
  wideWidth: 1005,
  calcWidth: function getWidth(value) {
    var width = parseFloat(this.getFloat.exec(value)[0]);
    // console.log("percent: " + value, "parsed: " + this.getFloat.exec(value)[0], "article width: " + this.width);
    // console.log(Math.round( this.isPercentage.test(String(value)) ? this.width * width / 100 : width));
    return Math.round( this.isPercentage.test(String(value)) ? this.width * width / 100 : width);
  },
  calcHeight: function getHeight(value) {
    var height = parseFloat(this.getFloat.exec(value)[0]);
    // console.log("percent: " + value, "parsed: " + this.getFloat.exec(value)[0], "article height: " + this.height);
    // console.log(Math.round( this.isPercentage.test(String(value)) ? this.height * height / 100 : height));
    return Math.round( this.isPercentage.test(String(value)) ? this.height * height / 100 : height);
  },
  isPercentage: /%$/,
  getFloat: /\d+\.?\d+/,
  margin: { top: 16, legendRight: 78, right: 390, bottom: 16, left: 148, wideRight: 75 },
  barWidth: 217 //0
};

hBird.yDownUpScale = d3.scale.linear()
                .domain([0, 100])
                .range([hBird.sizes.height - hBird.sizes.margin.bottom, hBird.sizes.margin.top]);
// TODO: setup proper scales <http://www.d3noob.org/2012/12/setting-scales-domains-and-ranges-in.html>
hBird.yScale = d3.scale.linear()
                .domain([0, 100])
                .range([0, hBird.sizes.height - hBird.sizes.margin.bottom]);

hBird.addValue = function(modifier) {
  "use strict";
  var firstValue = 0;
  if(modifier && !(modifier instanceof Function))
    throw "modifier must be a function";
  return function(item) {
    return modifier ?
            modifier(firstValue += item.value) :
            firstValue += item.value;
  }
};
hBird.yCoords = function(data) {
    "use strict";
  var intermed = [0].concat(
    data.map(
      hBird.addValue(hBird.yScale)
    )
  );
  return hBird.yCoords.coords = intermed.splice(0, intermed.length-1);
};

hBird.antiCollider = function(size, coords) {
  "use strict";
  var max = 692,
      min = 64;
  var collisions = coords.map(function(d, i, all) {
    var notLast = !hBird.isLast(all, i) // false if second last indice is reached
    return notLast && !(d + size*2 < all[i+1] || d - size*2 > all[i+1]);
  });
  console.log("detection ", collisions);
  if(!~collisions.indexOf(true))
    return coords;  // no collisions

  return coords.map(function(d, i, all) {
    var dis1, dis2;
    if(collisions[i] && !hBird.isLast(all, i)) {
      dis1 = Math.abs(d - all[i+1]);  // calc distance
      dis2 = Math.abs(d - all[i-1]);  // calc distance
      return dis1 < dis2 && d - size || dis1 > dis2 && d + size;
    }
    return d;
  });
}
hBird.antiCollider2 = function(size, coords) {
  "use strict";
  function Legend(pos) {
    this.y1 = pos;
    this.y2 = pos + this.height;
    this.cVector = [false, false];  
  }
  Legend.prototype.height = 49;
  Legend.prototype.upperBound = 692;
  Legend.prototype.lowerBound = 64;
  Legend.prototype.detection = function(legend) {
    var y1 = this.y1 - legend.y2,
        y2 = this.y2 - legend.y1;
    if( (y1 < 0 || y2 < 0) ) {
      this.cVector[0] = y1;
      this.cVector[1] = y2;
      return true;
    }
    return false;
  }
  Legend.prototype.move = function() {
    if(this.cVector[0] < 0) {
      console.log("moving up")
      this.y1 -= this.cVector[0];
      this.y2 -= this.cVector[0];
      this.cVector.splice(0, 2, false, false);
    }
    if(this.cVector[1] < 0) {
      console.log("moving down")
      this.y1 += this.cVector[1];
      this.y2 += this.cVector[1];
      this.cVector.splice(0, 2, false, false);
    }
  }

  var legends = coords.map(function(d) { return new Legend(d) });

var tmp = 1;
  while(hasCollisions(legends, detector)) {
console.log("Round " + tmp, "with " + legends.length + " legends");
console.dir(legends.map(function(d) { return d.cVector; }));
    legends.forEach(function(l) {
      l.move();
    });
tmp++;
  }

  return legends.map(function(l) {
    return l.y1;
  });

  function detector(legend1, legend2) {
    return legend1.detection(legend2);
    // var t = legend1.top - legend2.bottom,
    //     b = legend1.bottom - legend2.top;
    //   return (t < 0 || b < 0);
  }
  function hasCollisions(list, comparor) {
    return list.some(function(d, i, all) {
      return i === 0 ? false : comparor(d, all[i-1]);
    });
  }
}
hBird.median = function(i, coords) {
  "use strict";
  coords = coords || hBird.yCoords.coords;
  var next = i === coords.length-1 ? (hBird.sizes.height - hBird.sizes.margin.bottom) : coords[i+1];
  return (hBird.yCoords.coords[i] + next) / 2;
};

hBird.isLast = function(list, i) {
  return list.length-1 === i;
}

hBird.medianRange = function(coords) {
  "use strict";
  coords = coords || hBird.yCoords.coords;
  return coords.map(function(d, i, all) {
    return (hBird.isLast(all, i) ? d + hBird.sizes.height - hBird.sizes.margin.bottom : d + all[i+1]) / 2;
  });
}

hBird.indexOf = function(list, fn) {
  "use strict";
  for (var i = 0, len = list.length; i < len; i++) {
    if(fn(list[i], i, list))
      return i;
  }
  return -1;
};

hBird.initialize = function(cb, config) {
  "use strict";
      var yC = hBird.yCoords(config.data.values);
    var size = hBird.sizes,
      canvas = d3.select(".chart")
                 .append("svg");
  canvas.attr("viewBox", "0 0 " + size.width + " " + size.height)
        .attr("preserveAspectRatio", "xMinYMin meet");
    /* Gradients */
    var defs = canvas.append("defs"),
   tmp = defs.append("linearGradient")
                .attr("id", "gradient0");
   tmp.append("stop")
                .attr("stop-color", "rgb(63,108,122)")
                .attr("offset", "0");
   tmp.append("stop")
                .attr("stop-color", "rgb(81,140,163)")
                .attr("offset", "100%");
   tmp = defs.append("linearGradient")
                .attr("id", "gradient1");
   tmp.append("stop")
                .attr("stop-color", "rgb(0,62,86)")
                .attr("offset", "0");
   tmp.append("stop")
                .attr("stop-color", "rgb(0,87,114)")
                .attr("offset", "100%");
   tmp = defs.append("linearGradient")
                .attr("id", "gradient2");
   tmp.append("stop")
                .attr("stop-color", "rgb(12,137,175)")
                .attr("offset", "0");
   tmp.append("stop")
                .attr("stop-color", "rgb(13,163,221)")
                .attr("offset", "100%");
   tmp = defs.append("linearGradient")
                .attr("id", "gradient3");
   tmp.append("stop")
                .attr("stop-color", "rgb(81,137,150)")
                .attr("offset", "0");
   tmp.append("stop")
                .attr("stop-color", "rgb(107,184,224)")
                .attr("offset", "100%");
   tmp = defs.append("linearGradient")
                .attr("id", "gradient4");
   tmp.append("stop")
                .attr("stop-color", "rgb(81,81,81)")
                .attr("offset", "0");
   tmp.append("stop")
                .attr("stop-color", "rgb(109,109,109)")
                .attr("offset", "100%");
  /* drop-shadow */
    var filter = defs.append("filter")
              .attr("id", "dropshadow");
      filter.append("feGaussianBlur")
      .attr("in", "SourceAlpha")
      .attr("stdDeviation", 5);
    filter.append("feOffset")
        .attr("dx", -8)
        .attr("dy", 2)
        .attr("result", "offsetblur");
    tmp = filter.append("feComponentTransfer");
    tmp.append("feFuncA")
        .attr("type", "linear")
        .attr("slope", "0.25");
    tmp = filter.append("feMerge")
    tmp.append("feMergeNode")
    tmp.append("feMergeNode")
      .attr("in", "SourceGraphic");
      // cleanup
    defs = filter = tmp = null;

  var yAxis = d3.svg.axis()
    .scale(hBird.yDownUpScale)
    .tickSize(1)
    .orient("left")
    .tickValues([0, 20, 40, 60, 80, 100])
    .tickFormat(function(d) {
      return d + "%";
    });
  canvas.append("g")
    .attr("class", "y-axis")
    .attr("transform", "translate(60) scale(1, 0.98)") /* minimum to see text */
    .call(yAxis);

 // size.barWidth = size.width - size.margin.right - size.margin.left;
  var rect = canvas
    .append("g")
    .attr("class", "bar bar" + config.dataset.indexOf(config.data))
    .each(function() {
      // hack to get object constancy to work
      this.__data__ = config.data;
    })
    .attr("transform", "translate(" + size.margin.left + ")")
    .selectAll("g")
    .data(config.data.values)
    .enter()                
    .append("g") 
    .attr("class", "rects")              
    .append("rect")                
    .attr( 'filter', 'url(#dropshadow)' )
    .attr("fill", function(d, i) {
      return "url(#gradient" + i + ")";
    })
    .attr("y", function(d, i) {
      return yC[i];
    })
    .attr("height", function(d) {
      return hBird.yScale(d.value);
    })
    .attr("width", size.barWidth)
    .attr("class", function(d, i) {
      return "segment" + (i + 1);
    })
    .each(function(d, i) {
      this._yCoord = yC[i];
      this._datum = d;
    });
var lineToLabels = canvas
  .selectAll("g.rects")
  .data(config.data.values)
.append("line")
  .attr("x1", 0)
  .attr("y1", function(d, i) {
    return hBird.median(i);
  })
  .attr("x2", size.margin.legendRight)
  .attr("y2", function(d, i) {
    return hBird.median(i)-30;
  })
  .attr("stroke-width", 1)
  .attr("transform", "translate(" + size.barWidth + ")")
  .attr("class", "line-to-label")
  .each(function(d) {
    if(!d.value)
      d3.select(this).attr("opacity", 0);
  });
  var legend = canvas
    .selectAll("g.rects")
    .data(config.data.values)
  .append("g")
    .attr("transform", function(d, i) {
      var x = size.barWidth + size.margin.legendRight + 22;
      return "translate(" + x + ", " + hBird.median(i) + ")";
    })
    .each(function(d) {
      if(!d.value)
        d3.select(this).attr("opacity", 0);
    });
var percentage = legend.append("text")
  .text(function(d) {
    return Math.round(d.value) + "%";
  })
  .attr("y", -36)
  .attr("fill", "rgb(107,184,214)")
  .attr("class", "percentage-svg");
var icons = legend
  .attr("d", function(d) {
    var iconName = hBird.iconTable[d.label.trim()];
    var icon = d3.select(config.iconset).select("#" + iconName);
    icon.attr("class", iconName);
    switch (iconName) {
      case "drop":
        icon.attr("transform", "translate(5, -76)");
        break;
      case "pipette":
        icon.attr("transform", "translate(-26, -79)");
        break;                    
      case "question":
        icon.attr("transform", "translate(-10, -80)");
        break;
      case "pill":
        icon.attr("transform", "translate(-8, -80)");
        break;
      case "cross":
        icon.attr("transform", "translate(-8, -81)");
        break;
      default:
        icon.attr("transform", "translate(0, -69)");
    }
    this.appendChild(icon.node());
  });
  var labels = legend.append("text")
    .text(function(d) {
      return d.label;
    })
    .attr("y", -10)
    .attr("fill", "#000")
    .attr("text-anchor", "right")
    .attr("class", "label");

  var survivalRate = d3.select(".lost .number");
  survivalRate
    .datum(config.data["Survival Rate"])
    .transition()
    .duration(750)
    .delay(100)
    .tween("text", function(d) {
      var orig = +this.textContent,
          inter= d3.interpolateRound(orig, d);
      return function(t) {
        this.textContent = inter(t);
      }
    });
  var periodText = d3.select(".year")
    .text(config.data.period);

  config.canvas = canvas;
  config.rect = rect;
  config.periodText = periodText;
  config.legend = legend;
  config.labels = labels;
  config.percentage = percentage;
  config.lineToLabels = lineToLabels;
  config.survivalRate = survivalRate;
  cb(config);
};

hBird.cleanup = function(cb, config) {
  config.iconset = null;
  delete config.iconset;
  cb(config);
}

hBird.draw = function draw(cb, config) {
  "use strict";
  hBird.yCoords(config.data.values);
  // set bar g class
  config.canvas
        .select(".bar")
        .attr("class", "bar bar" + hBird.indexOf(config.dataset, function(d) {
          return d.period === config.data.period;
        }))
        .each(function() {
          // hack to get object constancy to work
          this.__data__ = config.data;
        });
  config.rect
        .data(config.data.values)
        .transition()
        .duration(750)
        .attrTween("y", function(d, i) {
          var inter = d3.interpolate(this._yCoord, hBird.yCoords.coords[i]);
          this._yCoord = hBird.yCoords.coords[i];
          return inter;
        })
        .attrTween("height", function(d) {
          var inter = d3.interpolate(hBird.yScale(this._datum.value), hBird.yScale(d.value));
          this._datum = d;
          return inter;
        })
        .each(function(d, i) {
          d3.select(this)
            .attr("height", hBird.yScale(d.value))
            .attr("y", hBird.yCoords.coords[i]);
        });
  
  config.periodText.text(config.data.period);

  var corrections = hBird.manuelLegendHandling(config)
  // var mRange = hBird.medianRange(hBird.yCoords.coords);
  // var collisions = hBird.antiCollider2(25, mRange);
  //var collisions;
  config.legend
        .data(config.data.values)
        .transition()
        .duration(750)
        .attr("transform", function(d, i) {
          var size = hBird.sizes;
          //var mRange = hBird.medianRange(collisions || hBird.yCoords.coords);

          //collisions = hBird.antiCollider2(25, mRange);
          //var collisions = hBird.yCoords.coords;
          var x = size.barWidth + size.margin.legendRight + 22;
          return "translate(" + x + ", " + (corrections[i][0] || hBird.median(i)) + ")";
// if(i===0) {          
// console.log("orig: ",hBird.yCoords.coords);
// console.log("medianer ",mRange);
// console.log("new ", collisions);
// }
//           return "translate(" + x + ", " + collisions[i] + ")";
        })
        .attr("opacity", function(d) {
          return d.value ? 1 : 0;
        })
        .select(".drop")
        .attr("transform", function(d) {
          return d.value === 100 ? "translate(2, -76)" : "translate(-14, -76)";
        });
  config.lineToLabels
        .data(config.data.values)
        .transition()
        .duration(750)
        .attr("y1", function(d, i) {
          return hBird.median(i);
        })
        .attr("y2", function(d, i) {
          // return the median between the current and next coord
          //return hBird.median(i, collisions)-30;
          return (corrections[i][1] || hBird.median(i) - 30 );
        })
        .attr("opacity", function(d) {
          return d.value ? 1 : 0;
        });
  config.percentage
        .data(config.data.values)
        .transition()
        .duration(750)
        .tween("text", function(d) {
          var orig = +this.textContent.replace("%", ""),
              //inter= d3.interpolateNumber(orig, d.value),
              //isInt= d.value % 1 === 0;
              inter= d3.interpolateRound(orig, d.value);
          return function(t) {
            var display = inter(t);
            //this.textContent = (isInt ? inter(t).toFixed(0) : inter(t).toFixed(1)) + "%";
            this.textContent = inter(t) + "%";
          }
        });  
  
  config.survivalRate
    .datum(config.data["Survival Rate"])
    .transition()
    .duration(750)
    .delay(260)
    .tween("text", function(d) {
      var orig = +this.textContent,
          inter= d3.interpolateRound(orig, d);
      return function(t) {
        this.textContent = inter(t);
      }
    });

  cb(config);
};

hBird.manuelLegendHandling = function(config) {
  var n = [null, null];
  switch(config.data.name) {
    case "set2":
      return [ n, [482, 452], [552, 522], [622, 592], [692, 662] ];
    case "set4":
      return [n, n, n, n, [682, 652]];
    case "set12":
      return [ [64,31], [126.51, 87.51], [186.21, 145.21], [251, 214.98], n];
    case "set14":
    case "set16":
      return [ [64,34], n, [134, 104], [204, 166],n]
    default: return [n,n,n,n,n];
  }
}

hBird.slideOut = function(cb, config) {
  "use strict";
  // remove legend and lines
  config.legend
        .attr("opacity", 0);
  config.lineToLabels
        .attr("opacity", 0);
  /* widen the svg area */
  d3.select(".chart")
    .classed("wide", true);

  var dataRange = config.dataset;
  var bars = config.canvas
    .selectAll("g.bar")
    .data(dataRange, function(d) {
      return d.name;
    })
    .enter()
    .append("g")
    .attr( 'filter', 'url(#dropshadow)' )
    //.attr("class", "bar");
    .attr("class", function(d) {
       return "bar bar" + config.dataset.indexOf(d);
    })
    .attr("transform", "translate(" + hBird.sizes.margin.left + ")")
    .each(function(d, i) {
      var yC;
      yC = hBird.yCoords(d.values);
      d3.select(this)
        .selectAll("rect")
        .data(d.values)
        .enter()
      .append("rect")
        .attr("fill", function(d, i) {
          return "url(#gradient" + i + ")";
        })
        .attr("width", hBird.sizes.barWidth)
        .attr("y", function(d, i) {
          return yC[i];
        })
        .attr("height", function(d) {
          return hBird.yScale(d.value);
        });
    });
  d3.select("svg").selectAll(".bar")
  // need to reiterate all DOM nodes with the data key func so d3 can sort them correctly
    .data(dataRange, function(d) { 
      return d.name;
    })
    .order()
    .transition()
    .delay(120)
    .duration(1000)
    //.ease(d3.ease("linear")) // TODO: find a nice animation ease
    .attr("transform", function(d, i) {
      var x = (i/config.counter.max * (hBird.sizes.wideWidth - hBird.sizes.margin.wideRight)) + hBird.sizes.margin.left;
      return "translate(" + x + ")";
    });
  cb(config);
}

/* like pickData but different */
hBird.selectData = function(data, selector) {
  if(selector instanceof Function) {
    return selector(data);
  } else if(typeof selector === "number") {
    return data.length > selector ? null : data[selector];
  }
}

hBird.slideIn = function(cb, config) {
  "use strict";
  var bars = config.canvas.selectAll(".bar");
  bars.transition()
    .duration(750)
    .attr("transform", "translate(" + hBird.sizes.margin.left + ")")
    .each("end", function(d, i) {
      if(d !== config.data)
        this.parentNode.removeChild(this);

      if(i+1 === config.counter.max) {
        // show legend and lines
        config.legend 
              .transition()
              .duration(400)
              .attr("opacity", function(d) {
                return Number(d.value > 0);
              });
        config.lineToLabels
              .transition()              
              .duration(400)
              .attr("opacity", function(d) {
                return Number(d.value > 0);
              });
        /* shrink the svg area */
        d3.select(".chart").classed("wide", false);
        cb(config);
      }
    });
}

hBird.log = function log(cb, data) {
  console.dir(data);
  cb(data);
};

hBird.previous = function previous(config, back, forw) {
  "use strict";
  return function() {
    // this.style.cursor = "progress";
    // drawing.transition().duration(0);
    hBird.move(-1, config, back, forw);
    // this.style.cursor = "pointer";
  }
};

hBird.next = function next(config, back, forw) {
  "use strict";
  return function() {
    // this.style.cursor = "progress";
    // drawing.transition().duration(0);
    hBird.move(1, config, back, forw);
    // this.style.cursor = "pointer";
  }
};

hBird.update = function(object, giveprop, takeprop, fn) {
  "use strict";
  var argumentsLength = arguments.length;
  return function update(cb, updated) {
    switch (argumentsLength) {
      case 1: object = updated; break;
      case 2: object[giveprop] = updated; break;
      case 3: object[giveprop] = updated[takeprop]; break;
      case 4: object[giveprop] = fn(updated[takeprop]); break;
      default: console.warn("hBird.update called with weird arguments");
    }
    cb(object);
  }
};

hBird.move = function(direction, config, backwards, forwards) {
  "use strict";
  var test = config.counter.c;
  test += direction;
  if(test < 0 || test >= config.counter.max) {
    console.warn("we got to the end!")
    return; // we at the end
  }
  !~direction ? hBird.defer(backwards(config)) : hBird.defer(forwards(config));
};

hBird.pickData = function(direction, counter) {
  "use strict";
  return function pickData(cb, config) {
    config.data = config.dataset[[!~direction ? --counter.c : ++counter.c]];
    cb(config);
  }
};


hBird.identity = function(fn) {
  return function(cb, data) {
    cb(fn(data));
  }
};
hBird.wait = function(wait) {
  "use strict";
  return function(cb, config) {
    setTimeout(function() {
      cb(config);
    }, wait)
  }
};

hBird.defer = function defer() {
  return hBird.wait(0);
}

hBird.noOp = function noOp(cb, config) { cb; };

hBird.changeClass = function(el, to) {
  var el = d3.selectAll(el),
      origClass = el.attr("class");
  return function(cb, config) {
    el.attr("class", origClass + " " + to);
    cb(config);
  }
};

(function() {
  "use strict";
  var counter = { c:-1, max: 0 },//TODO 0 from url .. -1 is a hack because forwardDataPicker otherwise will initialize the 1st dataset and not the 0th
      forwardDataPicker = hBird.pickData(1, counter),
      backwardsDataPicker = hBird.pickData(-1, counter);

  var svgUrl = function(cb, config) {
    hBird.update(config, "url")(cb, "images/iconset_stacked.svg");
  }

  /* performance measurement */
  // var start = function(cb, config) {
  //   config.start = window.performance.now();
  //   cb(config);
  // };
  // var end = function(cb, config) {
  //   console.log( window.performance.now() - config.start + "ms");
  //   cb(config);
  // };

  // setup program
  var config = new hBird.create({
    lineToLabels: undefined,
    legend: undefined,
    percentage: undefined,
    labels: undefined,
    periodText: undefined,
    url: "data/data11.json",
    dataset: undefined,
    data: undefined,
    canvas: undefined,
    rect: undefined,
    counter:counter,
    iconset: undefined,
    survivalRate: undefined,
    start: undefined
  });
  var initialRun = hBird.compose(
    // start,
    hBird.load,
    hBird.update(config, "dataset", "data"),
    forwardDataPicker,
    svgUrl,
    hBird.load,
    hBird.initialize,
    // end,
    hBird.update(counter, "max", "dataset", function(d) {
      return d.length;
    }),
    hBird.cleanup    
  );
  var forwards = hBird.composeCycle(
    // start,
    forwardDataPicker,
    hBird.draw,
    // end,
    hBird.noOp
  );
  var backwards = hBird.composeCycle(
    // start,
    backwardsDataPicker,
    hBird.draw,
    // end,
    hBird.noOp
  );

  var launchHistogram = hBird.composeCycle(
    // start,
    hBird.identity(function(config) {
      d3.select(".lost")
        .classed("slide-out left", true);
      d3.select(".controls")
        .classed("slide-out down", true);
      return config;
    }),
    hBird.slideOut,
    hBird.wait(1000),
    hBird.identity(function(config) {
      d3.select(".wide-icons")
        .classed("animate", true);
      return config;
    }),
    // end,
    // hBird.log,
    hBird.noOp
  );
  var cancelHistogram = hBird.composeCycle(
    // start,
    hBird.identity(function(config) {
      d3.select(".wide-icons")
        .classed("animate", false);
      return config;
    }),    
    hBird.wait(500),
    hBird.identity(function(config) {
      d3.select(".wide-icons")
        .classed("show", false);
      return config;
    }),
    hBird.slideIn,
    hBird.identity(function(config) {
     d3.select(".lost")
        .classed("slide-out left", false);
      d3.select(".controls")
        .classed("slide-out down", false);
      return config;
    }),
    // end,
    // hBird.log,
    hBird.noOp
  );

  // behavior
  d3.select(".previous")
    .on("click", hBird.previous(config, backwards, forwards));
  d3.select(".next")
    .on("click", hBird.next(config, backwards, forwards));

  var fullscreenBtn = d3.select(".fullscreen")
    .on(
      "click",
      toggle(
        function() {
          fullscreenBtn.classed("clicked", true);
          // Launch fullscreen for browsers that support it!
          launchFullScreen(document.documentElement);
        },
        function() {
          fullscreenBtn.classed("clicked", false);
          cancelFullscreen();
        }
      )
    );

  var histogramBtn = d3.select(".histogram")
    .on("click", toggle(function() {
      histogramBtn.classed("clicked", true);
      launchHistogram(config);
    },
    function() {
      histogramBtn.classed("clicked", false);
      cancelHistogram(config);
    }));

  // start program
  initialRun(config);

  /* util */
  function toggle (fn1, fn2) {
    var t = true;
    return function() {
      t ? fn1() : fn2();
      t = !t;
    }
  }

  /**
   *  Fullscreen stuff
   */
  //http://davidwalsh.name/fullscreen
  function launchFullScreen(element) {
    if(element.requestFullScreen) {
      element.requestFullScreen();
    } else if(element.mozRequestFullScreen) {
      element.mozRequestFullScreen();
    } else if(element.webkitRequestFullScreen) {
      element.webkitRequestFullScreen();
    }
  }
  function cancelFullscreen() {
    if(document.cancelFullScreen) {
      document.cancelFullScreen();
    } else if(document.mozCancelFullScreen) {
      document.mozCancelFullScreen();
    } else if(document.webkitCancelFullScreen) {
      document.webkitCancelFullScreen();
    }
  }
}());
