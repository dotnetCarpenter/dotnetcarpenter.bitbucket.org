var hBird = hBird || {};

hBird.config = function config(config) {
  for(var prop in config) {
    if(config.hasOwnProperty(prop))
      this[prop] = config[prop];
  }
}

hBird.load = function(cb, config) {
  d3.json(config.url, function(error, data) {
    if(error)
      return console.warn(error)
    config.data = data;
    cb(config);
  });
}

hBird.getValue = function(item) {
  return item.value;
}
hBird.addValue = function(modifier) {
  var firstValue = 0;
  if(modifier && !(modifier instanceof Function))
    throw "modifier must be a function"
  return function(item) {
    return modifier ?
            modifier(firstValue += item.value) :
            firstValue += item.value;
  }
}

hBird.initialize = function(cb, config) {
  var canvas = d3.select(document.body)
                .append("svg")
                .attr("width", 100)
                .attr("height", 400),
      color  = d3.rgb(188, 202, 0),
      yScale = d3.scale.linear()
                .domain([0, 100])
                .range([0, 400]),
     yCoords = [0].concat(
                  config.data.values.map(
                    hBird.addValue(yScale)
                  )
                ),
      yCoords = yCoords.splice(0, yCoords.length-1),
      stack  = d3.layout.stack()
                  .x(function() {
                    return 0;
                  })
                  .y(function(d, i) {
                    return yCoords[i];
                  })
                  .values(function(d) {
                    return d.values;
                  })
                  .offset(yCoords),
        rect = canvas.selectAll("rect")
               .data(config.data.values)
               .enter()
               .append("rect")
               .attr("y", function(d, i) {
                 return yCoords[i]
               })
               .attr("height", function(d, i) {
                 return yScale(d.value);
               })
               .attr("width", 100)
               .attr("fill", function(d, i) {
                  return color.brighter(i +1);
               });
                //.attr("transform", "translate(0, 400)"),
     /* keys   = Object.keys(data[0].values)
                .concat(Object.keys(data[0].values.Diagnosed)),
      */
  cb({
    data: config.data,
    canvas: canvas,
    yScale: yScale,
    rect: rect
  });
}

hBird.pickData = function(start) {
  var currentDataSet;
  return function(cb, config) {
    if(!currentDataSet) // if config doesn't have a data property then data is config
      currentDataSet = config.data;
    config.data = currentDataSet[start];
    cb(config);
  }
}

hBird.draw = function(cb, config) {
  config.canvas.selectAll.call(config.canvas, "rect")
               .data(config.data)
               .enter()
               .append("rect")
               .attr("y", function(d, i) {
                 return d;
               })
               .attr("x", function(d, i) {
                 return 0;
               })
               .attr("height", function(d) {
                 return config.yScale(d.values);
               })
               .attr("width", 100)
               .attr("fill", "#2d578b");
  cb(config);
}

hBird.log = function log(cb, data) {
  console.dir(data);
  cb(data);
}
var dataPicker = hBird.pickData(4);
var testRun = hBird.compose(hBird.load, dataPicker, hBird.log);
var firstTry = hBird.compose(hBird.load, dataPicker, hBird.initialize, hBird.log);
firstTry(new hBird.config({ url: "data/data4.json", data: null, rect: null }));
